using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    public float speed;
    public Rigidbody2D oRigidbody2D;

    public bool startBall = false;

    private float tempoPartida;
    public Text tempoPartida_txt;
    public bool jogoComecou;

    // Start is called before the first frame update
    void Start()
    {
        //transform.Translate(1.23f, 0,0);
    }

    // Update is called once per frame
    void Update()
    {
        startGame();
        JogoStart();

    }
    void startGame()
    {
        if (startBall == true)
        {
            oRigidbody2D.velocity = new Vector2(speed, 0) * Time.deltaTime;
            jogoComecou = true;
            startBall = /*!startBall;*/ false;
        }
    }
    void JogoStart()
    {

        if (jogoComecou == true)
        {
            tempoPartida = tempoPartida + Time.deltaTime;
            tempoPartida_txt.text = "Timer: " + tempoPartida.ToString("F1");

        }
    }

    public void alterarVelocidade(float newSpeed)
    {
        speed = newSpeed; //+ 600f;
        startBall = true;
    }

}

