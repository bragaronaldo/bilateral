using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{

    private float tempoPartida;
    // public bool jogoComecou;
    public Text tempoPartida_txt;
    // Start is called before the first frame update
    public bool jogoComecou = false;
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        JogoStart();

    }

    void JogoStart()
    {

        if (jogoComecou == true)
        {
            tempoPartida = tempoPartida + Time.deltaTime;
            tempoPartida_txt.text = "Timer: " + tempoPartida.ToString("F1");

        }
    }
}
