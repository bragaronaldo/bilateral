using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Contador : MonoBehaviour
{

    public Text scoreTxt;
    private int score;


    // Start is called before the first frame update
    void Start()
    {
        score = 0;
    }

    // Update is called once per frame

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("score") == true)
        {
            score++;
        }
    }


    void Update()
    {
        scoreTxt.text = "Counters: " + score.ToString();

    }
}
