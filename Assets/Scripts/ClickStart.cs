using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickStart : MonoBehaviour
{
    // Start is called before the first frame update

    public Button startButton;
    // public GameObject botao1;
    public bool comecou = false;
    void Start()
    {
        startButton.onClick = new Button.ButtonClickedEvent();
        startButton.onClick.AddListener(() => clique());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void clique()
    {
        Debug.Log("start");
        Player player = GetComponent<Player>();
        player.speed = 600f;
        player.startBall = true;
        comecou = true;
    }
}
