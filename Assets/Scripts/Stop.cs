using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stop : MonoBehaviour
{

    public Button stop;
    // Start is called before the first frame update
    void Start()
    {
        stop.onClick = new Button.ButtonClickedEvent();
        stop.onClick.AddListener(() => stopBall());
    }

    // Update is called once per frame
    void Update()
    {

    }

    void stopBall()
    {
        Debug.Log("Stop");
        Player player = GetComponent<Player>();
        player.speed = 0f;
        player.jogoComecou = false; //!player.jogoComecou;
        player.startBall = true;
        
    }
}
